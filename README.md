**Lightweight 802.11 driver and applications** 

The repository is private at the moment, and I have no intention to publish it now, but review / test is welcome. Just drop me a line to access the private repository. 

Strongly welcome: user space application ideas and collaborations of any form :-)


**Status**

Completed:
== WiFi driver for rt73usb, tested on kernel 3.13, 3.17, and 3.18 

On-going dev:
== ath9k_htc driver
== user space MLME and applications


**In the repository**

Core work:
- Lw_WiFi_driver_rt2x00: WiFi driver for rt73usb hardware: tested working on kernel 3.13/17/18
- Lw_WiFi_driver_ath: WiFi driver for ath9k_htc hardware: on-going development
- U_mlme:  user space 802.11 MLME, and other user-space applications: on-going dev

Other toy projects: 
- Static_router: static router for Stanford networking project: completed
- toy_driver_usb: usb driver skeleton: completed
- toy_driver_eth: Ethernet driver based on LDD: completed
- arm_kernel: an ARM bootloader and kernel. Interesting but no bandwidth to work on it for now. 


**Links**

https://bitbucket.org/wirefree/lw_wifi_driver_rt2x00
https://bitbucket.org/wirefree/lw_wifi_driver_ath
https://bitbucket.org/wirefree/u_mlme
https://bitbucket.org/wirefree/static_router
https://bitbucket.org/wirefree/toy_driver_usb
https://bitbucket.org/wirefree/toy_driver_eth
https://bitbucket.org/wirefree/arm_kernel